/* eslint-disable no-undef */
import FavoriteRestaurantIdb from '../src/scripts/data/database';
import * as TestFactories from './helpers/testFactories';

describe('Liking A Restaurant', () => {
  const addLikeButtonContainer = () => {
    document.body.innerHTML = '<div id="likeButtonContainer"></div>';
  };

  beforeEach(async() => {
    addLikeButtonContainer();
  });

  it('should show the favorite button when the restaurant has not been liked before', async() => {
    await TestFactories.createLikeButtonPresenterWithRestaurant({ id: 1 });

    const favoriteButton = document.querySelector('[aria-label="favorite this restaurant"]');
    expect(favoriteButton).toBeTruthy();
  });

  it('should not show the unfavorite button when the restaurant has not been liked before', async() => {
    await TestFactories.createLikeButtonPresenterWithRestaurant({ id: 1 });

    // harusnya button unfavorite tidak muncul kalau restaurant belum di favorite
    const favoriteButton = document.querySelector('[aria-label="unfavorite this restaurant"]');
    expect(favoriteButton).toBeFalsy();
  });

  it('should be able to like the restaurant', async() => {
    await TestFactories.createLikeButtonPresenterWithRestaurant({ id: 1 });

    const likeButtonEl = document.querySelector('#likeButton');
    likeButtonEl.dispatchEvent(new Event('click'));

    const restaurant = await FavoriteRestaurantIdb.getRestaurant(1);

    expect(restaurant).toEqual({ id: 1 });

    FavoriteRestaurantIdb.deleteRestaurant(1);
  });

  it('should not add a restaurant again when its already favorited', async() => {
    await TestFactories.createLikeButtonPresenterWithRestaurant({ id: 1 });

    // add restaurant with ID : 1 to favorited list
    await FavoriteRestaurantIdb.putRestaurant({ id: 1 });

    // simulate user click the favorite button
    const likeButtonEl = document.querySelector('#likeButton');
    likeButtonEl.dispatchEvent(new Event('click'));

    // check if no duplicated favorited restaurant
    const checkRestaurant = await FavoriteRestaurantIdb.getAllRestaurants();

    expect(checkRestaurant).toEqual([{ id: 1 }]);

    FavoriteRestaurantIdb.deleteRestaurant(1);
  });

  it('should not add a restaurant when it has no id', async() => {
    await TestFactories.createLikeButtonPresenterWithRestaurant({ });

    const likeButtonEl = document.querySelector('#likeButton');
    likeButtonEl.dispatchEvent(new Event('click'));

    const checkRestaurant = await FavoriteRestaurantIdb.getAllRestaurants();

    expect(checkRestaurant).toEqual([]);
  });
});
