/* eslint-disable no-undef */
import FavoriteRestaurantIdb from '../src/scripts/data/database';
import * as TestFacroties from './helpers/testFactories';

const addLikeButtonContainer = () => {
  document.body.innerHTML = '<div id="likeButtonContainer"></div>';
};

describe('Unfavorite A Restaurant', () => {
  beforeEach(async() => {
    addLikeButtonContainer();
    await FavoriteRestaurantIdb.putRestaurant({ id: 1 });
  });

  afterEach(async() => {
    await FavoriteRestaurantIdb.deleteRestaurant(1);
  });

  it('should display unfavorite button when the restaurant has been liked', async() => {
    await TestFacroties.createLikeButtonPresenterWithRestaurant({ id: 1 });

    const unfavoriteButtonEl = document.querySelector('[aria-label="unfavorite this restaurant"]');
    expect(unfavoriteButtonEl)
      .toBeTruthy();
  });

  it('should not display favorite button when the restaurant has been liked', async() => {
    await TestFacroties.createLikeButtonPresenterWithRestaurant({ id: 1 });

    const unfavoriButtonEl = document.querySelector('[aria-label="favorite this restaurant"]');
    expect(unfavoriButtonEl)
      .toBeFalsy();
  });

  it('should be able to remove unfavorite restaurant from the list', async() => {
    await TestFacroties.createLikeButtonPresenterWithRestaurant({ id: 1 });

    const unfavoriButtonEl = document.querySelector('[aria-label="unfavorite this restaurant"]');
    unfavoriButtonEl.dispatchEvent(new Event('click'));

    const restaurant = await FavoriteRestaurantIdb.getAllRestaurants();
    expect(restaurant).toEqual([]);
  });

  it('should not throw error if the unfavorite restaurant is not in the list', async() => {
    await TestFacroties.createLikeButtonPresenterWithRestaurant({ id: 1 });

    await FavoriteRestaurantIdb.deleteRestaurant(1);

    const unfavoriButtonEl = document.querySelector('[aria-label="unfavorite this restaurant"]');
    unfavoriButtonEl.dispatchEvent(new Event('click'));

    const restaurant = await FavoriteRestaurantIdb.getAllRestaurants();
    expect(restaurant).toEqual([]);
  });
});
