import LikeButtonPresenter from '../../src/scripts/utils/like-button-presenter';
import consumerReviewInitiator from '../../src/scripts/utils/my-review-presenter';
import FavoriteRestaurantIdb from '../../src/scripts/data/database';
import TheRestaurantSource from '../../src/scripts/data/restaurant-source';

const createLikeButtonPresenterWithRestaurant = async(restaurantParam) => {
  const likeButtonContainer = document.querySelector('#likeButtonContainer');
  await LikeButtonPresenter.init({
    likeButtonContainer: likeButtonContainer,
    favoriteRestaurant: FavoriteRestaurantIdb,
    restaurant: restaurantParam
  });
};

const createConsumerReviewFormPresenterWithRestaurant = async(param) => {
  const myReviewContainer = document.querySelector('#myReviewContainer');
  const consumerReviewContainer = document.querySelector('#consumerReviewContainer');

  await consumerReviewInitiator.init({
    myReviewContainer: myReviewContainer,
    id: param.id || null,
    consumerReviewContainer: consumerReviewContainer,
    theRestaurantSource: TheRestaurantSource
  });
};

export {
  createLikeButtonPresenterWithRestaurant,
  createConsumerReviewFormPresenterWithRestaurant
};
