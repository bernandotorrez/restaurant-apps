/* eslint-disable no-undef */
import TheRestaurantList from '../../src/scripts/data/restaurant-source';

const itActsAsPostReviewRestaurantModel = async(restaurant) => {
  it('should be able to post a review to a restaurant', async() => {
    const restaurantData = await restaurant;
    const restaurants = restaurantData.restaurants;

    const dataTestOne = {
      id: restaurants[0].id,
      name: 'test name from karma',
      review: 'test review from karma'
    };

    const postReviewTestOne = await TheRestaurantList.postReview(dataTestOne);

    expect(postReviewTestOne.message)
      .toEqual('success');

    const dataTestTwo = {
      id: restaurants[1].id,
      name: 'test name from karma',
      review: 'test review from karma'
    };

    const postReviewTestTwo = await TheRestaurantList.postReview(dataTestTwo);

    expect(postReviewTestTwo.message)
      .toEqual('success');
  });
};

export { itActsAsPostReviewRestaurantModel };
