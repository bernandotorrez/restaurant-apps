/* eslint-disable no-undef */
import { itActsAsPostReviewRestaurantModel } from './contracts/consumerReviewContract';
import TheRestaurantList from '../src/scripts/data/restaurant-source';

describe('Post Review Contract Test Implementation', () => {
  itActsAsPostReviewRestaurantModel(TheRestaurantList.restaurantList());
});
