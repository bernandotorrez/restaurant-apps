/* eslint-disable no-undef */
import TheRestaurantList from '../src/scripts/data/restaurant-source';
import * as TestFactories from './helpers/testFactories';

describe('Posting Review a Restaurant', () => {
  const consumerReviewContainer = () => {
    document.body.innerHTML = ' <div id="myReviewContainer"></div>';
  };

  beforeEach(async() => {
    consumerReviewContainer();
  });

  it('should show review form', async() => {
    await TestFactories.createConsumerReviewFormPresenterWithRestaurant({ id: '' });

    const consumerReviewForm = document.querySelector('.form-group');
    expect(consumerReviewForm).toBeTruthy();
  });

  it('should show red border or input validation when Reviewer Name is Empty', async() => {
    await TestFactories.createConsumerReviewFormPresenterWithRestaurant({ id: '' });

    const reviewerNameEl = document.querySelector('#nameValue');
    reviewerNameEl.value = '';

    const postReviewButton = document.querySelector('#reviewButton');
    postReviewButton.dispatchEvent(new Event('click'));

    expect(reviewerNameEl.classList.contains('error-validation')).toBeTrue();
  });

  it('should show red border or input validation when Review Input is Empty', async() => {
    await TestFactories.createConsumerReviewFormPresenterWithRestaurant({ id: '' });

    // Fill value of Review Name
    const reviewerNameEl = document.querySelector('#nameValue');
    reviewerNameEl.value = 'name';

    const reviewValueEl = document.querySelector('#reviewValue');
    reviewValueEl.value = '';

    const postReviewButton = document.querySelector('#reviewButton');
    postReviewButton.dispatchEvent(new Event('click'));

    expect(reviewValueEl.classList.contains('error-validation')).toBeTrue();
  });

  it('should be able to post a review to a restaurant', async() => {
    const restaurantData = await TheRestaurantList.restaurantList();
    const restaurants = restaurantData.restaurants;

    const data = {
      id: restaurants[0].id,
      name: 'test name from karma',
      review: 'test review from karma'
    };

    const postReview = await TheRestaurantList.postReview(data);
    expect(postReview.message).toEqual('success');
  });

  it('should not add a review when it has no id', async() => {
    const data = {
      id: '',
      name: 'test name from karma',
      review: 'test review from karma'
    };

    const postReview = await TheRestaurantList.postReview(data);
    expect(postReview.message).toEqual('failed to add review');
  });
});
