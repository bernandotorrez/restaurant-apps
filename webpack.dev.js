const { merge } = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common');

module.exports = merge(common, {
  mode: 'development',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 3002,
    clientLogLevel: 'error'
  },
  watch: true,
  watchOptions: {
    ignored: /node_modules/
  }
});
