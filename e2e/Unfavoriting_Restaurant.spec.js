/* eslint-disable no-undef */
Feature('Unfavoriting a restaurant');

Before((I) => {
  I.amOnPage('/#/favorite');
});

Scenario('showing favorited Restaurant', (I) => {
  I.seeElement('.restaurant-item');
});

Scenario('Unfavoriting one Restaurant', async(I) => {
  I.seeElement('.restaurant-item');
  const firstRestaurant = locate('.restaurant-item__title a').first();

  I.scrollTo(firstRestaurant);
  I.click(firstRestaurant);

  I.seeElement('#likeButton');
  I.click('#likeButton');

  I.amOnPage('/#/favorite');
  I.dontSeeElement('.restaurant-item');
});
