/* eslint-disable no-undef */
Feature('Reviewing a Restaurant');

Before((I) => {
  I.amOnPage('/');
});

Scenario('Showing detail one Restaurant', (I) => {
  I.seeElement('.restaurant-item');

  const firstRestaurant = locate('.restaurant-item__title a').first();

  I.scrollTo(firstRestaurant);
  I.click(firstRestaurant);

  I.seeInCurrentUrl('/#/detail');
});

Scenario('Showing Input Validation on Your Name Field', (I) => {
  I.seeElement('.restaurant-item');

  const firstRestaurant = locate('.restaurant-item__title a').first();

  I.scrollTo(firstRestaurant);
  I.click(firstRestaurant);

  I.scrollTo('#myReviewContainer');
  I.click('#reviewButton');
  I.seeElement('.error-validation');
});

Scenario('Showing Input Validation on Your Review Field', (I) => {
  I.seeElement('.restaurant-item');

  const firstRestaurant = locate('.restaurant-item__title a').first();

  I.scrollTo(firstRestaurant);
  I.click(firstRestaurant);

  I.scrollTo('#myReviewContainer');
  I.fillField('#nameValue', 'My Name From Codecept');
  I.click('#reviewButton');
  I.seeElement('.error-validation');
});

Scenario('Posting a Review to Restaurant', async(I) => {
  I.seeElement('.restaurant-item');

  const firstRestaurant = locate('.restaurant-item__title a').first();

  I.scrollTo(firstRestaurant);
  I.click(firstRestaurant);

  I.seeInCurrentUrl('/#/detail');

  I.scrollTo('#myReviewContainer');
  I.fillField('#nameValue', 'My Name From Codecept');
  I.fillField('#reviewValue', 'My Review From Codecept');

  I.click('#reviewButton');
  I.see('Your Review Successfully Sent!', '.review-success');
});
