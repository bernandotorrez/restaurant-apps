/* eslint-disable no-undef */
const assert = require('assert');

Feature('Liking Restaurant');

Before((I) => {
  I.amOnPage('/#/favorite');
});

Scenario('showing empty favorited Restaurant', (I) => {
  I.see('Oops, Your Favorite Restaurant is Empty, Let\'s Favorite One', '.has-no-favorite');
});

Scenario('Favoriting one Restaurant', async(I) => {
  I.see('Oops, Your Favorite Restaurant is Empty, Let\'s Favorite One', '.has-no-favorite');

  I.amOnPage('/');

  I.seeElement('.restaurant-item');

  const firstRestaurant = locate('.restaurant-item__title a').first();
  const firstRestaurantTitle = await I.grabTextFrom(firstRestaurant);
  I.scrollTo(firstRestaurant);
  I.click(firstRestaurant);

  I.seeElement('#likeButton');
  I.click('#likeButton');

  I.amOnPage('/#/favorite');
  I.seeElement('.restaurant-item');

  const favoritedRestaurantTitle = await I.grabTextFrom('.restaurant-item__title a');

  assert.strictEqual(firstRestaurantTitle, favoritedRestaurantTitle);
});
