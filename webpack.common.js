const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const { ProgressPlugin } = require('webpack');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const CompressionPlugin = require('compression-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const WorkboxPlugin = require('workbox-webpack-plugin');
const CONFIG = {
  BASE_URL: 'https://dicoding-restaurant-api.el.r.appspot.com/list',
  DETAIL_BASE_URL: 'https://dicoding-restaurant-api.el.r.appspot.com/detail/',
  IMAGE_BASE_URL: 'https://dicoding-restaurant-api.el.r.appspot.com/images/'
};
const devMode = process.env.NODE_ENV !== 'production';

const handler = (percentage, message, ...args) => {
  console.info(`${(percentage * 100).toFixed()}% ${message}`);
};

module.exports = {
  entry: path.resolve(__dirname, 'src/scripts/index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          'css-loader?url=false'
        ]
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true,
              disable: true
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/templates/index.html'),
      filename: 'index.html'
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'src/public/'),
          to: path.resolve(__dirname, 'dist/'),
          globOptions: {
            ignore: ['**/heros/**']
          }
        }
      ]
    }),
    new ProgressPlugin(handler),
    new WebpackPwaManifest({
      name: 'Nongkrong Asik App',
      short_name: 'NoSik',
      description: 'Aplikasi Cari Tempat Nongkrong Asik',
      background_color: '#ffffff',
      theme_color: '#012756',
      start_url: 'index.html',
      display: 'standalone',
      icons: [
        {
          src: path.resolve('src/public/icon.png'),
          sizes: [96, 128, 192, 256, 384, 512],
          purpose: 'maskable'
        }
      ],
      crossorigin: 'anonymous'
    }),
    new WorkboxPlugin.GenerateSW({
      clientsClaim: true,
      skipWaiting: true,
      runtimeCaching: [
        {
          urlPattern: new RegExp('/'),
          handler: 'StaleWhileRevalidate'
        },
        {
          urlPattern: new RegExp(CONFIG.BASE_URL),
          handler: 'StaleWhileRevalidate'
        },
        {
          urlPattern: new RegExp(CONFIG.DETAIL_BASE_URL),
          handler: 'NetworkFirst'
        },
        {
          urlPattern: new RegExp(CONFIG.IMAGE_BASE_URL),
          handler: 'StaleWhileRevalidate'
        },
        {
          urlPattern: new RegExp('https://fonts.googleapis.com/'),
          handler: 'CacheFirst'
        },
        {
          urlPattern: new RegExp('https://use.fontawesome.com/'),
          handler: 'CacheFirst'
        },
        {
          urlPattern: new RegExp('https://fonts.gstatic.com/s/materialicons/v53/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2'),
          handler: 'CacheFirst'
        }
      ]
    }),
    new CompressionPlugin(),
    new MiniCssExtractPlugin({
      filename: devMode ? '[name].css' : '[name].[hash].css',
      chunkFilename: devMode ? '[id].css' : '[id].[hash].css'
    })
  ],
  optimization: {
    minimize: true,
    minimizer: [new TerserJSPlugin({
      cache: true,
      sourceMap: true,
      parallel: true
    }), new OptimizeCSSAssetsPlugin({})]
  }
};
