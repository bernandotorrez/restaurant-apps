import 'regenerator-runtime'; /* for async await transpile */
import '../styles/main.css';
import '../styles/responsive.css';
import 'lazysizes';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import App from './views/app';

const menu = document.getElementById('menu');
const main = document.getElementById('maincontent');
const drawer = document.getElementById('drawer');

// document.getElementById('hero').style.backgroundImage = "url('./images/heros/hero-image-small.jpg')";

const app = new App({
  button: menu,
  drawer: drawer,
  content: main
});

window.addEventListener('hashchange', () => {
  app.renderPage();
});

window.addEventListener('load', () => {
  app.renderPage();
});

if('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('./service-worker.js').then(registration => {
      console.log('SW registered: ', registration);
    }).catch(registrationError => {
      console.log('SW registration failed: ', registrationError);
    });
  });
}
