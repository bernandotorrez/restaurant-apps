const CONFIG = {
  KEY: '12345',
  BASE_URL: 'https://dicoding-restaurant-api.el.r.appspot.com/',
  BASE_IMAGE_URL: 'https://dicoding-restaurant-api.el.r.appspot.com/images/medium/',
  BASE_IMAGE_URL_SMALL: 'https://dicoding-restaurant-api.el.r.appspot.com/images/small/',
  DEFAULT_LANGUAGE: 'en-us',
  CACHE_NAME: 'NoSik-Restaurant-V1',
  MSG_ERROR: 'Oops, something went wrong',
  MSG_NOT_FOUND: 'Oops, page not found',
  MSG_FORBIDDEN: 'Forbidden Access',
  MSG_FAV_EMPTY: 'Oops, Your Favorite Restaurant is Empty, Let\'s Favorite One',
  MSG_POST_SUCCESS: 'Your Review Successfully Sent!',
  DATABASE_NAME: 'nosik-apps-database',
  DATABASE_VERSION: 1,
  OBJECT_STORE_NAME: 'NoSik-Restaurant-V1'
};

export default CONFIG;
