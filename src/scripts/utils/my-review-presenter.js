import {
  createMyReviewTemplate,
  createConsumerReviewAfterPostTemplate,
  createErrorLoadTemplate,
  createSuccessPostReviewTemplate
} from '../views/templates/template-creator';

const myReviewPresenter = {
  async init({ myReviewContainer, id, consumerReviewContainer, theRestaurantSource }) {
    this._myReviewContainer = myReviewContainer;
    this._id = id;
    this._consumerReviewContainer = consumerReviewContainer;
    this._theRestaurantSource = theRestaurantSource;
    await this._renderButton();
  },

  async _renderButton() {
    this._myReviewContainer.innerHTML = createMyReviewTemplate(this.id);

    const reviewButton = document.querySelector('#reviewButton');

    const nameValue = document.querySelector('#nameValue');
    const reviewValue = document.querySelector('#reviewValue');
    reviewButton.addEventListener('click', () => {
      this._postReview({ nameValue: nameValue, reviewValue: reviewValue });
    });
  },

  async _postReview({ nameValue, reviewValue }) {
    if(nameValue.value === '') {
      nameValue.style.border = '2px solid red';
      nameValue.classList.add('error-validation');
      nameValue.focus();
    } else if(reviewValue.value === '') {
      reviewValue.style.border = '2px solid red';
      reviewValue.classList.add('error-validation');
      reviewValue.focus();
    } else {
      nameValue.style.border = '';
      reviewValue.style.border = '';
      nameValue.classList.remove('error-validation');
      reviewValue.classList.remove('error-validation');

      const data = {
        id: this._id,
        name: nameValue.value,
        review: reviewValue.value
      };

      const postReview = await this._theRestaurantSource.postReview(data);

      if(postReview.message === 'success') {
        const postMessageContainer = document.querySelector('#post-message');
        postMessageContainer.innerHTML = createSuccessPostReviewTemplate();
        this._consumerReviewContainer.innerHTML = createConsumerReviewAfterPostTemplate(postReview);

        this._emptyValue({ nameValue: nameValue, reviewValue: reviewValue });
      } else {
        this._consumerReviewContainer.innerHTML = createErrorLoadTemplate();
      }

      return postReview;
    }
  },

  _emptyValue({ nameValue, reviewValue }) {
    nameValue.value = '';
    reviewValue.value = '';
  }
};

export default myReviewPresenter;
