import { createBackButtonTemplate } from '../views/templates/template-creator';

const backButtonInitiator = {
  async init({ backButtonContainer }) {
    this.backButtonContainer = backButtonContainer;
    await this._renderButton();
  },

  async _renderButton() {
    this.backButtonContainer.innerHTML = createBackButtonTemplate();

    const backButton = document.querySelector('#backButton');
    backButton.addEventListener('click', () => {
      this._backHref();
    });
  },

  _backHref() {
    window.history.back();
  }
};

export default backButtonInitiator;
