const GlobalHelper = {
  loopDataCategories(categories) {
    const category = categories.map((category) => {
      return ` ${category.name}`;
    });

    return category;
  },

  loopDataMenusFoods(menus) {
    const menuFood = menus.foods.map((menu) => {
      return ` ${menu.name}`;
    });

    return menuFood;
  },

  loopDataMenusDrinks(menus) {
    const menuDrink = menus.drinks.map((menu) => {
      return ` ${menu.name}`;
    });

    return menuDrink;
  }

};

export default GlobalHelper;
