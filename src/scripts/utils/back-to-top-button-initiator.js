import { createBackToTopButtonTemplate } from '../views/templates/template-creator';

const backToTopButtonInitiator = {
  async init({ backTopTopButtonContainer }) {
    this._backToTopButtonContainer = backTopTopButtonContainer;
    await this._renderButton();
  },

  async _renderButton() {
    this._backToTopButtonContainer.innerHTML = createBackToTopButtonTemplate();

    const backToTopButton = document.querySelector('#backToTopButton');
    backToTopButton.addEventListener('click', () => {
      this._scrollToTop();
    });

    window.onscroll = () => {
      this._showHideButton(backToTopButton);
    };
  },

  _showHideButton(backToTopButton) {
    if(document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
      backToTopButton.style.display = 'block';
    } else {
      backToTopButton.style.display = 'none';
    }
  },

  _scrollToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
};

export default backToTopButtonInitiator;
