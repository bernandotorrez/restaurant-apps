import UrlParser from '../../routes/url-parser';
import TheRestaurantSource from '../../data/restaurant-source';
import {
  createRestaurantDetailTemplate,
  createErrorLoadTemplate,
  createErrorNotFoundTemplate,
  createConsumerReviewTemplate,
  createSkeletonRestaurantTemplate
} from '../templates/template-creator';
import LikeButtonPresenter from '../../utils/like-button-presenter';
import backButtonInitiator from '../../utils/back-button-initiator';
import myReviewPresenter from '../../utils/my-review-presenter';
import FavoriteRestaurantIdb from '../../data/database';

const Detail = {
  async render() {
    return `

    <section class="content">
        <div class="explore" id="detail">
            <h1 class="explore__label" tabindex="0">Detail Restaurant</h1>
            
              <div id="error" align="center"></div>
            
              <div id="restaurant" class="detail-restaurant">
                ${createSkeletonRestaurantTemplate(1)}
              </div>

              <div align="center">
                <div id="backButtonContainer"></div>
              </div>
              
              <div id="consumerReviewContainer" style="text-align: left;"></div>
            </div>

        </div>
        
        <div id="myReviewContainer"></div>

        <div id="likeButtonContainer"></div>
    </section>
    `;
  },

  async afterRender() {
    const url = UrlParser.parseActiveUrlWithoutCombiner();
    const dataRestaurant = await TheRestaurantSource.detailRestaurant(url.id);
    const restaurant = dataRestaurant.restaurant;
    const message = dataRestaurant.message;

    const backButtonContainer = document.querySelector('#backButtonContainer');
    await backButtonInitiator.init({
      backButtonContainer: backButtonContainer
    });

    const restaurantContainer = document.querySelector('#restaurant');

    if(message === 'success') {
      this._hideLoading(restaurantContainer);

      restaurantContainer.innerHTML = createRestaurantDetailTemplate(restaurant);
      LikeButtonPresenter.init({
        likeButtonContainer: document.querySelector('#likeButtonContainer'),
        favoriteRestaurant: FavoriteRestaurantIdb,
        restaurant: {
          id: restaurant.id,
          name: restaurant.name,
          description: restaurant.description,
          city: restaurant.city,
          address: restaurant.address,
          pictureId: restaurant.pictureId,
          categories: restaurant.categories,
          menus: restaurant.menus,
          rating: restaurant.rating,
          consumerReviews: restaurant.consumerReviews
        }
      });

      const consumerReviewContainer = document.querySelector('#consumerReviewContainer');
      consumerReviewContainer.innerHTML += createConsumerReviewTemplate(restaurant.consumerReviews);

      const myReviewContainer = document.querySelector('#myReviewContainer');
      myReviewPresenter.init({
        myReviewContainer: myReviewContainer,
        id: restaurant.id,
        consumerReviewContainer: consumerReviewContainer,
        theRestaurantSource: TheRestaurantSource
      });
    } else if(message === 'error') {
      this._hideLoading(restaurantContainer);

      const errorContainer = document.querySelector('#error');
      errorContainer.innerHTML += createErrorLoadTemplate();
    } else if(message === 'not found') {
      this._hideLoading(restaurantContainer);

      const errorContainer = document.querySelector('#error');
      errorContainer.innerHTML += createErrorNotFoundTemplate();
    }

    document.getElementById('detail').scrollIntoView();
  },

  _hideLoading(restaurantContainer) {
    restaurantContainer.innerHTML = '';
  }
};

export default Detail;
