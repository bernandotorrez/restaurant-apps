import TheRestaurantList from '../../data/restaurant-source';
import { createRestaurantItemTemplate, createErrorLoadTemplate, createSkeletonRestaurantTemplate } from '../templates/template-creator';
import backToTopButtonInitiator from '../../utils/back-to-top-button-initiator';

const Home = {
  async render() {
    const html = `
    <section class="content">
        <div class="explore">
            <h1 class="explore__label" tabindex="0">Explore Restaurant</h1>

              <div id="error" align="center"></div>
            
              <div class="restaurants" id="restaurant-list">
                
              ${createSkeletonRestaurantTemplate(20)}
              
              </div>
        </div>
    </section>

    <div id="backToTopButtonContainer"></div>
    `;

    return html;
  },

  async afterRender() {
    const restaurantList = await TheRestaurantList.restaurantList();
    const message = restaurantList.message;
    const backToTopButtonContainer = document.querySelector('#backToTopButtonContainer');

    backToTopButtonInitiator.init({
      backTopTopButtonContainer: backToTopButtonContainer
    });

    const restaurantContainer = document.querySelector('#restaurant-list');

    if(message === 'success') {
      this._hideLoading(restaurantContainer);
      restaurantList.restaurants.forEach((restaurant) => {
        restaurantContainer.innerHTML += createRestaurantItemTemplate(restaurant);
      });
    } else {
      this._hideLoading(restaurantContainer);
      const errorContainer = document.querySelector('#error');
      errorContainer.innerHTML += createErrorLoadTemplate();
    }
  },

  _hideLoading(restaurantContainer) {
    restaurantContainer.innerHTML = '';
  }
};

export default Home;
