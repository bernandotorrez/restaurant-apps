import {
  createRestaurantItemTemplate,
  createErrorLoadTemplate,
  createEmptyFavoriteTemplate,
  createSkeletonRestaurantTemplate
} from '../templates/template-creator';
import backToTopButtonInitiator from '../../utils/back-to-top-button-initiator';
import FavoriteRestaurantIdb from '../../data/database';

const Home = {
  async render() {
    const html = `
    <section class="content">
        <div class="explore">
            <h1 class="explore__label" tabindex="0">Favorite Restaurant</h1>

              <div id="error" align="center"></div>
            
              <div class="restaurants" id="restaurant-list">
                ${createSkeletonRestaurantTemplate(20)}
              </div>
        </div>
    </section>

    <div id="backToTopButtonContainer"></div>
    `;

    return html;
  },

  async afterRender() {
    const backToTopButtonContainer = document.querySelector('#backToTopButtonContainer');

    backToTopButtonInitiator.init({
      backTopTopButtonContainer: backToTopButtonContainer
    });

    const restaurantContainer = document.querySelector('#restaurant-list');

    try {
      this._hideLoading(restaurantContainer);

      const restaurantList = await FavoriteRestaurantIdb.getAllRestaurants();

      if(restaurantList.length > 0) {
        restaurantList.forEach((restaurant) => {
          restaurantContainer.innerHTML += createRestaurantItemTemplate(restaurant);
        });
      } else {
        const errorContainer = document.querySelector('#error');
        errorContainer.innerHTML += createEmptyFavoriteTemplate();
      }
    } catch (err) {
      this._hideLoading(restaurantContainer);

      const errorContainer = document.querySelector('#error');
      errorContainer.innerHTML += createErrorLoadTemplate();
    }
  },

  _hideLoading(restaurantContainer) {
    restaurantContainer.innerHTML = '';
  }
};

export default Home;
