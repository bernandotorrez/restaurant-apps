import CONFIG from '../../globals/config';
import GlobalHelper from '../../utils/global-helper';

const createRestaurantDetailTemplate = (restaurant) => `

<article class="detail-restaurant-item">
<span tabindex="0" class="card-rating">
⭐️
<span>${restaurant.rating}</span>

</span>
    <img class="restaurant-item__thumbnail__detail lazyload"
        src="./images/placeholder.png"
        alt="${restaurant.name} - ${restaurant.city}"
        title="${restaurant.name} - ${restaurant.city}"
        tabindex="0"
        crossorigin="anonymous"
        data-src="${restaurant.pictureId ? CONFIG.BASE_IMAGE_URL + restaurant.pictureId : './images/placeholder.png'}"
        >

    <div class="restaurant-item__content">
        
        <h1 class="restaurant-item__title"><a>${restaurant.name} - ${restaurant.city}</a></h1>

        <h4 class="restaurant-item__description__detail__h4" tabindex="0">Address</h4>
        <p class="restaurant-item__description__detail" tabindex="0">${restaurant.address}</p>

        <h4 class="restaurant-item__description__detail__h4" tabindex="0">Categories</h4>
        <p class="restaurant-item__description__detail" tabindex="0">${GlobalHelper.loopDataCategories(restaurant.categories)}</p>

        <h4 class="restaurant-item__description__detail__h4" tabindex="0">Menu (Foods)</h4>
        <p class="restaurant-item__description__detail" tabindex="0">${GlobalHelper.loopDataMenusFoods(restaurant.menus)}</p>

        <h4 class="restaurant-item__description__detail__h4" tabindex="0">Menu (Drinks)</h4>
        <p class="restaurant-item__description__detail" tabindex="0">${GlobalHelper.loopDataMenusDrinks(restaurant.menus)}</p>
        
        <h4 class="restaurant-item__description__detail__h4" tabindex="0">Overview</h4>
        <p class="restaurant-item__description__detail" tabindex="0">${restaurant.description}</p>
        
    </div>
</article>

`;

const createSkeletonRestaurantTemplate = (count) => {
  let template = '';

  for (let i = 0; i < count; i += 1) {
    template += `
      <article class="restaurant-item">
            <img class="restaurant-item__thumbnail"
                src="./images/placeholder.png"
                alt="Skeleton"
                tabindex="0"
                crossorigin="anonymous"
                >
          
            <div class="restaurant-item__content">
                
                <h1 class="restaurant-item__title skeleton">Lorem ipsum dolor sit amet</h1>
                
                <p class="restaurant-item__description truncate skeleton" tabindex="0">Lorem ipsum dolor sit amet</p>
                
            </div>
    </article>
  `;
  }
  return template;
};

const createErrorLoadTemplate = () => `
  <center><h2 class="has-error">${CONFIG.MSG_ERROR}</h2></center>
`;

const createErrorNotFoundTemplate = () => `
  <center><h2 class="has-not-found">${CONFIG.MSG_NOT_FOUND}</h2></center>
`;

const createEmptyFavoriteTemplate = () => `
  <center><h2 class="has-no-favorite">${CONFIG.MSG_FAV_EMPTY}</h2></center>
`;

const createSuccessPostReviewTemplate = () => `
  <center><h2 class="has-success review-success" style="color: green;">${CONFIG.MSG_POST_SUCCESS}</h2></center>
`;

const createRestaurantItemTemplate = (restaurant) => `

  <article class="restaurant-item">
        <span tabindex="0" class="card-rating">
        ⭐️
        <span>${restaurant.rating}</span>
        
        </span>
          
            <img class="restaurant-item__thumbnail lazyload"
                src="./images/placeholder.png"
                alt="${restaurant.name} - ${restaurant.city}"
                title="${restaurant.name} - ${restaurant.city}"
                tabindex="0"
                crossorigin="anonymous"
                data-src="${restaurant.pictureId ? CONFIG.BASE_IMAGE_URL_SMALL + restaurant.pictureId : './images/placeholder.png'}"
                >
          
            <div class="restaurant-item__content">
                
                <h1 class="restaurant-item__title"><a href="#/detail/${restaurant.id}#detail">${restaurant.name} - ${restaurant.city}</a></h1>
                
                <p class="restaurant-item__description truncate" tabindex="0">${restaurant.description}</p>
                
            </div>
    </article>
  `;

const createFavoriteButtonTemplate = () => `
  <button aria-label="favorite this restaurant" id="likeButton" class="empty-heart" tabIndex="0">
    ♡
  </button>
`;

const createUnfavoriteButtonTemplate = () => `
  <button aria-label="unfavorite this restaurant" id="likeButton" class="filled-heart" tabIndex="0">
    ❤
  </button>
`;

const createBackToTopButtonTemplate = () => `
  <button aria-label="back to top" id="backToTopButton" class="back-to-top" tabIndex="0">
    <i class="arrow up" aria-hidden="true"></i>
  </button>
`;

const createBackButtonTemplate = () => `
  <button aria-label="back" id="backButton" class="back-button" tabindex="0">
    <i class="arrow left" aria-hidden="true"></i> &nbsp; Back
  </button>
`;

const createConsumerReviewTemplate = (reviews) => {
  let html = `
    <h1 class="restaurant-item__title__detail__review" align="left" tabindex="0"><a>Consumer Review</a></h1>
    <p></p>
  `;

  reviews.forEach((review) => {
    if(typeof review.name !== 'undefined') {
      html += `
      
      <div class="review">
        <label class="label truncate" for="Consumer Name" tabindex="0">${review.name} - ${review.date}</label>
      
        <p class="restaurant-item__description__detail__review truncate" tabindex="0"> ${review.review}</p>
      </div>  
      `;
    }
  });

  return html;
};

const createMyReviewTemplate = (id) => `
<div id="post-message"></div>
<div class="form-group" align="center">
  <input type="hidden" id="idvalue" value="${id}"/>
  <label class="label" for="nameValue" tabindex="0">Your Name</label>

  <input class="form-control" id="nameValue" placeholder="Your Name" tabindex="0">

  <label class="label" for="reviewValue" tabindex="0">Your Review</label>

  <textarea class="form-control" id="reviewValue" rows="4" placeholder="Your Review" tabindex="0"></textarea>

    <button align="center" aria-label="back" id="reviewButton" class="back-button" tabindex="0">
      Submit Review
    </button>
 
</div>
`;

const createConsumerReviewAfterPostTemplate = (review) => {
  let html = `
    <h1 class="restaurant-item__title__detail__review" align="left" tabindex="0"><a>Consumer Review</a></h1>
    <p></p>
  `;

  review.customerReviews.forEach((review) => {
    if(typeof review.name !== 'undefined') {
      html += `
      
      <div class="review">
        <label class="label truncate" for="Consumer Name" tabindex="0">${review.name} - ${review.date}</label>
      
        <p class="restaurant-item__description__detail__review truncate" tabindex="0"> ${review.review}</p>
      </div>  
      `;
    }
  });

  return html;
};

export {
  createRestaurantItemTemplate,
  createRestaurantDetailTemplate,
  createFavoriteButtonTemplate,
  createUnfavoriteButtonTemplate,
  createErrorLoadTemplate,
  createBackToTopButtonTemplate,
  createBackButtonTemplate,
  createErrorNotFoundTemplate,
  createEmptyFavoriteTemplate,
  createConsumerReviewTemplate,
  createMyReviewTemplate,
  createConsumerReviewAfterPostTemplate,
  createSuccessPostReviewTemplate,
  createSkeletonRestaurantTemplate
};
